package com.zipcode.solution.service;

import static org.junit.Assert.assertEquals;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import com.zipcode.solution.dto.ZipCode;

public class ZipCodeProcessorServiceTest {
	
	ZipCodeProcessorService service = new ZipCodeProcessorService();
	
	@Test
	public void splitZipCodeWithSpaceTest() {
		String input = "12345,12345 23456,34567";
		String[] zipCodeArr = service.splitZipCodeWithSpace(input);
		assertEquals(zipCodeArr.length, 2);
	}
	
	@Test
	public void splitZipCodeWithCommaTest() {
		String input = "12345,12345";
		String[] zipCodeArr = service.splitZipCodeWithComma(input);
		assertEquals(zipCodeArr.length, 2);
	}
	
	
	@Test
	public void testcompareUpperBound() {
		int firstUpperBound = 234;
		int secondUpperBound = 274;
		int result = service.compareUpperBound(firstUpperBound,  secondUpperBound);
		assertEquals(result, secondUpperBound);
	}
	
	@Test
	public void testcompareFirstUpperBound() {
		int firstUpperBound = 1234;
		int secondUpperBound = 274;
		int result = service.compareUpperBound(firstUpperBound,  secondUpperBound);
		assertEquals(result, firstUpperBound);
	}
	
	@Test
	public void mergeZipCodesTestWithMerger() {
		ZipCode zip1 = new ZipCode();
		zip1.setLowerBound(12345);
		zip1.setUpperBound(12355);
		
		ZipCode zip2 = new ZipCode();
		zip2.setLowerBound(12345);
		zip2.setUpperBound(12355);
		
		List<ZipCode> zipList = new LinkedList<>();
		zipList.add(zip1);
		zipList.add(zip2);
		Collections.sort((List<ZipCode>) zipList);
		List<ZipCode> mergedZipList = service.mergeZipCodes(zipList);
		assertEquals(mergedZipList.size(), 1);
		
	}
	
	@Test
	public void mergeZipCodesTestWithMerger2() {
		ZipCode zip1 = new ZipCode();
		zip1.setLowerBound(12345);
		zip1.setUpperBound(12355);
		
		ZipCode zip2 = new ZipCode();
		zip2.setLowerBound(12341);
		zip2.setUpperBound(12359);
		
		List<ZipCode> zipList = new LinkedList<>();
		zipList.add(zip1);
		zipList.add(zip2);
		Collections.sort((List<ZipCode>) zipList);
		List<ZipCode> mergedZipList = service.mergeZipCodes(zipList);
		assertEquals(mergedZipList.size(), 1);
		assertEquals(mergedZipList.get(0).getLowerBound(), 12341);
		assertEquals(mergedZipList.get(0).getUpperBound(), 12359);
		
	}
	
	@Test
	public void mergeZipCodesTestWithMergerMixed() {
		ZipCode zip1 = new ZipCode();
		zip1.setLowerBound(12345);
		zip1.setUpperBound(12355);
		
		ZipCode zip2 = new ZipCode();
		zip2.setLowerBound(12341);
		zip2.setUpperBound(12349);
		
		List<ZipCode> zipList = new LinkedList<>();
		zipList.add(zip1);
		zipList.add(zip2);
		Collections.sort((List<ZipCode>) zipList);
		List<ZipCode> mergedZipList = service.mergeZipCodes(zipList);
		assertEquals(mergedZipList.size(), 1);
		assertEquals(mergedZipList.get(0).getLowerBound(), 12341);
		assertEquals(mergedZipList.get(0).getUpperBound(), 12355);
		
	}
	
	@Test
	public void getZipCodeListTest() {
		String zipCodeString = "12345,12347 23456,23457";
		List<ZipCode> zipCodeList = service.getZipCodeList(zipCodeString);
		assertEquals(zipCodeList.size(), 2);
	}
	
	@Test
	public void getZipCodeListMergedTest() {
		String zipCodeString = "12345,12347 12343,12347";
		List<ZipCode> zipCodeList = service.getZipCodeList(zipCodeString);
		assertEquals(zipCodeList.size(), 2);
	}
	
	@Test
	public void getZipCodeListWithExceptionTest() {
		String zipCodeString = "123425,12347 23456,23457";
		try {
			List<ZipCode> zipCodeList = service.getZipCodeList(zipCodeString);
		}catch(IllegalArgumentException ex) {
			assertEquals("IllegalArgumentException", ex.getClass().getSimpleName());
		}
	}
	
	@Test
	public void getZipCodeListMergedExceptionTest() {
		String zipCodeString = "123425,12347 12343,12347";
		try {
			List<ZipCode> zipCodeList = service.getZipCodeList(zipCodeString);
		}catch(IllegalArgumentException ex) {
			assertEquals("IllegalArgumentException", ex.getClass().getSimpleName());
		}
		
	}

}
