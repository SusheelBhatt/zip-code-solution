package com.zipcode.solution.service.validation;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.zipcode.solution.validation.ZipCodeValidator;

public class ZipCodeValidatorTest {
	
	ZipCodeValidator validator = new ZipCodeValidator();
	
	
	
	@Test
	public void validateZipCodeSuccessTest() {
		String input[] = {"12345", "12345"};
		boolean response = validator.validateZipCode(input);
		assertEquals(true, response);
	}
	
	
	@Test
	public void validateZipCodeRangeSuccessTest() {
		String input[] = {"12345","12347"};
		boolean response = validator.validateZipCodeRange(input);
		assertEquals(true, response);
		
	}

	@Test
	public void validateZipCodeTest() {
		String input[] = {"12345"};
		try {
			validator.validateZipCode(input);
		}catch(IllegalArgumentException ex) {
			assertEquals("IllegalArgumentException", ex.getClass().getSimpleName());
		}
	}
	
	@Test
	public void validateZipCodeLengthTest() {
		String input[] = {"1234,1234"};
		try {
			validator.validateZipCode(input);
		}catch(IllegalArgumentException ex) {
			assertEquals("IllegalArgumentException", ex.getClass().getSimpleName());
		}
	}
	
	@Test
	public void validateZipCodeLengthForLowerBoundTest() {
		String input[] = {"1234,12345"};
		try {
			validator.validateZipCode(input);
		}catch(IllegalArgumentException ex) {
			assertEquals("IllegalArgumentException", ex.getClass().getSimpleName());
		}
	}
	
	@Test
	public void validateZipCodeLengthForUpperBoundTest() {
		String input[] = {"12345,1234"};
		try {
			validator.validateZipCode(input);
		}catch(IllegalArgumentException ex) {
			assertEquals("IllegalArgumentException", ex.getClass().getSimpleName());
		}
	}
	
	@Test
	public void validateZipCodeRangeTest() {
		String input[] = {"12345","12343"};
		try {
			validator.validateZipCodeRange(input);
		}catch(IllegalArgumentException ex) {
			assertEquals("IllegalArgumentException", ex.getClass().getSimpleName());
		}
	}

}
