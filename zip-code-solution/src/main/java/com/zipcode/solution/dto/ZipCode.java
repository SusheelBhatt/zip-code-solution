package com.zipcode.solution.dto;

import java.util.Objects;

public class ZipCode implements Comparable<ZipCode>{

	private int lowerBound;
	
	private int upperBound;

	public int getLowerBound() {
		return lowerBound;
	}

	public void setLowerBound(int lowerBound) {
		this.lowerBound = lowerBound;
	}

	public int getUpperBound() {
		return upperBound;
	}

	public void setUpperBound(int upperBound) {
		this.upperBound = upperBound;
	}
	
	public String toString() {
		return " Lower Bound: "+ this.lowerBound +" Upper Bound: " + this.upperBound;
	}
	
	public int hashCode() { 
		int hash = 7;
		hash = 79 * hash + Objects.hashCode(this.lowerBound); 
		return hash;
	}
	
	public boolean equals(Object obj) { 
		if (obj == null) {
			return false; 
		} 
		if (getClass() != obj.getClass()){
			return false;
		} 
		final ZipCode other = (ZipCode) obj; 
		if (!Objects.equals(this.lowerBound, other.lowerBound)) { 
			return false; 
		} 
		return true;
	}


	@Override
	public int compareTo(ZipCode o) {
		 return Integer.compare(this.getLowerBound(), o.getLowerBound());
	} 
}
