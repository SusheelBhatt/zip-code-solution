package com.zipcode.solution.validation;

public class ZipCodeValidator {
	
	public boolean validateZipCode(String[] zipCodeString) {
		if(zipCodeString.length != 2) {
			throw new IllegalArgumentException("Please fix the zip code bounds.. Zip Code Shoudl have lower bound and upper bound" + zipCodeString);
		}
		return true;
	}
	
	public boolean validateZipCodeLength(String[] zipCodeString) {
		if(zipCodeString[0].length() !=5 && zipCodeString[0].length() !=5) {
			throw new IllegalArgumentException("zip code length has to be 5.."+zipCodeString);
		}
		return true;
	}
	
	public boolean validateZipCodeRange(String[] zipCodeString) {
		if(Integer.parseInt(zipCodeString[0]) > Integer.parseInt(zipCodeString[1])) {
			throw new IllegalArgumentException("lowe bound should not be greater than upper bound.."+zipCodeString);
		}
		return true;
	}


}
