package com.zipcode.solution;

import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import com.zipcode.solution.dto.ZipCode;
import com.zipcode.solution.service.ZipCodeProcessorService;

public class Startup {
	
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in); 
		String zipCodeBounds = scan.nextLine();
		scan.close();
		
		ZipCodeProcessorService zipcodeProcessorService = new ZipCodeProcessorService();
		if(zipCodeBounds != null) {
			List<ZipCode> zipCodeList = zipcodeProcessorService.getZipCodeList(zipCodeBounds);
			Collections.sort((List<ZipCode>) zipCodeList);
			List<ZipCode> mergedCodeList =  zipcodeProcessorService.mergeZipCodes(zipCodeList);
			displayZipCodes(mergedCodeList);
		}
		
		
	}
	
	public static void displayZipCodes(List<ZipCode> zipCodeList) {
		for(ZipCode zip : zipCodeList) {
			System.out.println("["+ zip.getLowerBound()+","+zip.getUpperBound()+"]");
		}
	}

}
