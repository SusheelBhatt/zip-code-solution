package com.zipcode.solution.service;

import java.util.LinkedList;
import java.util.List;

import com.zipcode.solution.dto.ZipCode;
import com.zipcode.solution.validation.ZipCodeValidator;

public class ZipCodeProcessorService {
	
	ZipCodeValidator validator;
	
	public ZipCodeProcessorService(){
		validator = new ZipCodeValidator();
	}
	
	public List<ZipCode> getZipCodeList(String zipCodeString){
		String[] zipCodeArr = splitZipCodeWithSpace(zipCodeString);
		List<ZipCode> zipCodeList = prepareZipCodeList(zipCodeArr);
		return zipCodeList;
	}
	
	
	private List<ZipCode> prepareZipCodeList(String[] zipCodeArr){
		List<ZipCode> zipCodeList = new LinkedList<>();
		for(String zip : zipCodeArr) {
			zipCodeList.add(prepareZipCode(zip));
		}
		return zipCodeList;
	}
	
	
	public ZipCode prepareZipCode(String zipCodeString) {
		String[] bounds = zipCodeString.replaceAll("\\[|\\]", "").split(",");
		if(validator.validateZipCode(bounds) && validator.validateZipCodeLength(bounds) && validator.validateZipCodeRange(bounds)) {
			ZipCode zip = new ZipCode();
			zip.setLowerBound(Integer.parseInt(bounds[0]));
			zip.setUpperBound(Integer.parseInt(bounds[1]));
			return zip;
		}
		return null;
	}
	
	public String[] splitZipCodeWithSpace(String zipCodes) {
		return zipCodes.split(" ");
	}
	
	public String[] splitZipCodeWithComma(String zipCodes) {
		return zipCodes.split(",");
	}
	
	public List<ZipCode> mergeZipCodes(List<ZipCode> zipCodeList){
		List<ZipCode> mergedZipCodeList = new LinkedList<>();
		
		ZipCode zipCode = null;
		for(ZipCode zip : zipCodeList) {
			
			if(zipCode == null) {
				zipCode= zip;
			}
			
			if (zipCode.getUpperBound() >= zip.getLowerBound()) {
				zipCode.setUpperBound(compareUpperBound(zipCode.getUpperBound(), zip.getUpperBound()));
			}else{
				mergedZipCodeList.add(zipCode);
				zipCode = zip;
			}
		}
		mergedZipCodeList.add(zipCode);
		return mergedZipCodeList;
	}
	
	public int compareUpperBound(int firstUpperBound, int secondUpperBound) {
		if(firstUpperBound > secondUpperBound) {
			return firstUpperBound;
		}
		return secondUpperBound;
	}
	
	
}
